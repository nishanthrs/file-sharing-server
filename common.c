/**
 * Networking
 */
#include "common.h"
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

ssize_t read_all_from_socket(int socket, char *buffer, size_t count) {
    if (socket == -1)
        return -1;

    // Implement error handling here
    errno = 0; // Reset to 0 in case it was called before
    ssize_t bytes_read_from_socket;
    ssize_t total_bytes_read = 0;
    while ((size_t) total_bytes_read < count) {
        bytes_read_from_socket = read(socket, buffer + total_bytes_read, count - total_bytes_read); // Keep reading from socket as bytes of data keep coming in until full amo    unt of data is read
        if (bytes_read_from_socket == 0) // Finished reading from socket
            return total_bytes_read;
        else if (bytes_read_from_socket == -1) {
           if (errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK) { // Read failed which means socket was down, so keep try    ing by resetting errno
                errno = 0;
                continue;
           }
           else { // Could not read from socket at all
               perror("Read all to socket error\n"); 
               return -1;
           }
        }
        else // Everything worked out
            total_bytes_read += bytes_read_from_socket;
    }
    
    return total_bytes_read;
}

ssize_t write_all_to_socket(int socket, const char *buffer, size_t count) {
    if (socket == -1)
        return -1;

    // Implement error handling here
    errno = 0; // Reset to 0 in case it was called before
    ssize_t bytes_written_to_socket;
    ssize_t total_bytes_written = 0;
    while ((size_t) total_bytes_written < count) {
        bytes_written_to_socket = write(socket, buffer + total_bytes_written, count - total_bytes_written); // Keep writing to socket as bytes of data keep coming in until fu    ll amount of data is written
        if (bytes_written_to_socket == 0) // Finished reading from socket
            return total_bytes_written; 
        else if (bytes_written_to_socket == -1) {
            if (errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK) { // Read failed which means socket was down, so keep try    ing by resetting errno
                errno = 0;
                continue;
            }
            else { // Could not read from socket at all
                perror("Write all to socket error\n");
                return -1;
            }
        }   
        else // Everything worked out
            total_bytes_written += bytes_written_to_socket;
    }

    return total_bytes_written;
}