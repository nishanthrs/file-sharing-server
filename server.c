/**
 * Networking
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <dirent.h>
#include <signal.h>
#include <ftw.h>

#include "vector.h"
#include "format.h"
#include "common.h"

#define MAX_CLIENTS 1000
#define BUFFER_SIZE 1024
#define CLIENT_REQUEST_SIZE 1024

typedef struct client {
    int socket_fd;
    int client_request_new_header;
} client;

client clients[MAX_CLIENTS];
int curr_client_num;

char* temp_dirname;
vector* server_files;

void create_temp_directory() {
    char template[] = "XXXXXX";
    strcpy(temp_dirname, mkdtemp(template));
    if (temp_dirname == NULL) {
        fprintf(stderr, "Error creating temp dir\n");
        exit(1);
    }
    print_temp_directory(temp_dirname);
}

int unlink_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf) {
    int rv = remove(fpath);
    if (rv)
        perror(fpath);

    return rv;
}

int rmrf(char *path) {
    return nftw(path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
}

void clean_up_temp_directory() {
    if (rmrf(temp_dirname) == -1) {
        perror("Error removing temp dir\n");
        return;
    } 
}

/**
 * Algorithm of LIST server response:
 * Perform ls on temporary directory using 
 */
void handle_list_request(struct epoll_event *e) {
    int socket_fd = e->data.fd;
    
    int num_bytes_written = write_all_to_socket(socket_fd, "OK\n", strlen("OK\n"));
    if (num_bytes_written == -1) {
        close(socket_fd);
        return;
    } 

    size_t num_bytes_to_send = 0;
    for (size_t i = 0; i < vector_size(server_files); i++) {
        char* server_file = vector_get(server_files, i);
        if (i != vector_size(server_files) - 1)
            num_bytes_to_send += strlen(server_file) + 1;
        else
            num_bytes_to_send += strlen(server_file);
    }
    
    write_all_to_socket(socket_fd, (char*) &num_bytes_to_send, sizeof(size_t));

    for (size_t i = 0; i < vector_size(server_files); i++) {
        char* server_file = vector_get(server_files, i);
        if (i != vector_size(server_files) - 1) {
            write_all_to_socket(socket_fd, server_file, strlen(server_file));
            write_all_to_socket(socket_fd, "\n", strlen("\n"));
        }
        else
            write_all_to_socket(socket_fd, server_file, strlen(server_file));
    } 
}

/**
 * Algorithm of PUT server response:
 * 1. Create temporary directory using mkdtemp
 * 1. Get the name of the what the remote file is supposed to be called
 * 2. Create file and open it for appending
 * 3. Get size of data supposed to be read from socket_fd (added to remote file) from client request
 * 4. Read from socket_fd in small buffers of size 1024 in a while loop and keep writing that to the remote file created earlier
 * 5. If read function finishes and number of bytes read is not equal to expected number of bytes to be read, then write error message to socket and delete remote file
 * 6. Else, write successful response to socket_fd
 */
void handle_put_request(struct epoll_event *e, char* client_request_header) {
    int socket_fd = e->data.fd; 

    // fprintf(stderr, "Client request header: %s\n", client_request_header);

    char* remote_filename = client_request_header + 4;
    if (strlen(remote_filename) <= 1) {
        write_all_to_socket(socket_fd, "ERROR\n", strlen("ERROR\n"));
        write_all_to_socket(e->data.fd, err_bad_request, strlen(err_bad_request));
        return;
    }

    remote_filename[strlen(remote_filename)] = '\0'; // Get rid of \n escape character

    // fprintf(stderr, "File on server: %s\n", remote_filename);

    char path[strlen(temp_dirname) + strlen(remote_filename) + 2];
    strcpy(path, temp_dirname);
    strcat(path, "/");
    strcat(path, remote_filename);
    FILE* remote_file = fopen(path, "w+");
    // fprintf(stderr, "File: %p\n", remote_file);

    // size_t client_request_num_bytes[sizeof(size_t)];
    // bzero(client_request_num_bytes, sizeof(size_t));
    
    char* client_request_num_bytes_char = malloc(sizeof(size_t));

    read_all_from_socket(socket_fd, client_request_num_bytes_char, sizeof(size_t));
    // fprintf(stderr, "Num bytes parsed char: %s\n", client_request_num_bytes_char);

    char client_request[CLIENT_REQUEST_SIZE];
    bzero(client_request, CLIENT_REQUEST_SIZE);

    size_t num_bytes_read = 0;
    size_t bytes_to_be_read = *((size_t*) (client_request_num_bytes_char));
    while (num_bytes_read < bytes_to_be_read) {
        size_t bytes_read_from_socket = (size_t) read_all_from_socket(socket_fd, client_request, CLIENT_REQUEST_SIZE);
        if (bytes_read_from_socket == 0) // Finished reading from socket
            break;
        else if ((int) bytes_read_from_socket == -1) {
            if (errno == EINTR || errno == EAGAIN) { // Read failed which means socket was down, so keep trying by resetting errno
                errno = 0;
                continue;
            }
            else // Could not read from socket at all
                break;
        }
        
        num_bytes_read += bytes_read_from_socket;

        int bytes_written = write_all_to_socket(fileno(remote_file), client_request, bytes_read_from_socket);
        if (bytes_written == -1) { // Something fucked up
            close(socket_fd);
            return;
        }
        // fprintf(stderr, "Bytes written: %d\n", bytes_written); 
    }
    if (num_bytes_read < bytes_to_be_read) {
        print_too_little_data();
        if (access(path, F_OK) != -1)
            remove(path);
        write_all_to_socket(socket_fd, "ERROR\n", strlen("ERROR\n"));
        write_all_to_socket(socket_fd, err_bad_file_size, strlen(err_bad_file_size));
        return;
    }
    else if (num_bytes_read > bytes_to_be_read) {
        print_received_too_much_data();
        if (access(path, F_OK) != -1)
            remove(path);
        write_all_to_socket(socket_fd, "ERROR\n", strlen("ERROR\n"));
        write_all_to_socket(socket_fd, err_bad_file_size, strlen(err_bad_file_size));
        return;
    }

    // fprintf(stderr, "About to write OK response\n");
    // If found in vector already, don't add
    int found = 0;
    for (size_t i = 0; i < vector_size(server_files); i++) {
        char* server_file = vector_get(server_files, i);
        if (strcmp(server_file, remote_filename) == 0) {
            found = 1;
            break;
        }
    }
    if (found == 0)
        vector_push_back(server_files, remote_filename);
    // fprintf(stderr, "Pushed to files vector: %s\n", remote_filename);
    write_all_to_socket(socket_fd, "OK\n", strlen("OK\n"));
    // fprintf(stderr, "Wrote OK response\n");
}

/**
 * Algorithm of GET server response:
 * 1. Get the name of the remote file
 * 2. Detect if file exists on server by using access call
 * 3. If it exists, continue with step 5
 * 4. If it does not exist, then write err_no_such_file response to socket
 * 5. Get file size using stat and write that to the socket as a char*
 * 5. Read from file in small buffers of size 1024 in a while loop and keep writing that to the socket (use appropriate error handling)
 */
void handle_get_request(struct epoll_event *e, char* client_request_header) {
    int socket_fd = e->data.fd;
    
    char* remote_filename = client_request_header + 4;
    if (strlen(remote_filename) <= 1) {
        write_all_to_socket(socket_fd, "ERROR\n", strlen("ERROR\n"));
        write_all_to_socket(e->data.fd, err_bad_request, strlen(err_bad_request));
        return;
    }

    remote_filename[strlen(remote_filename)] = '\0'; // Get rid of \n escape character

    char path[strlen(temp_dirname) + strlen(remote_filename) + 2];
    strcpy(path, temp_dirname);
    strcat(path, "/");
    strcat(path, remote_filename);
 
    size_t client_request_num_bytes[sizeof(size_t)];
    bzero(client_request_num_bytes, sizeof(size_t));

    FILE* remote_file = fopen(path, "r");
    // fprintf(stderr, "Does it reach here?\n");
    if (remote_file == NULL) {
        // fprintf(stderr, "FILE IS NULL\n");
        write_all_to_socket(socket_fd, "ERROR\n", strlen("ERROR\n"));
        write_all_to_socket(socket_fd, err_no_such_file, strlen(err_no_such_file));
        return;
    }

    char client_request[CLIENT_REQUEST_SIZE];
    bzero(client_request, CLIENT_REQUEST_SIZE);
 
    write_all_to_socket(socket_fd, "OK\n", strlen("OK\n"));

    struct stat remote_file_info;
    size_t remote_file_size = 0;
    if (stat(path, &remote_file_info) != -1)
        remote_file_size = (size_t) remote_file_info.st_size;

    write_all_to_socket(socket_fd, (char*) &remote_file_size, sizeof(size_t));

    size_t num_bytes_read = 0;
    size_t bytes_to_be_read = remote_file_size;
    while (num_bytes_read < bytes_to_be_read) {
        size_t bytes_read_from_socket = (size_t) read_all_from_socket(fileno(remote_file), client_request, CLIENT_REQUEST_SIZE);
        if (bytes_read_from_socket == 0) // Finished reading from socket
            break;
        else if ((int) bytes_read_from_socket == -1) {
            if (errno == EINTR || errno == EAGAIN) { // Read failed which means socket was down, so keep trying by resetting errno
                errno = 0;
                continue;
            }
            else // Could not read from socket at all
                break;
        }

        num_bytes_read += bytes_read_from_socket;

        int num_bytes_written = write_all_to_socket(socket_fd, client_request, bytes_read_from_socket);
        if (num_bytes_written == -1) { // Something fucked up
            close(socket_fd);
            return;
        }
    }

}

/**
 * Algorithm of DELETE server response:
 * 1. Get the name of the remote file that client wants to delete
 * 2. Detect if file exists on server by using access call
 * 3. If it exists, then call remove on remote file name and write successful response to socket
 * 4. If it does not exist, then write error response to socket
 */
void handle_delete_request(struct epoll_event *e, char* client_request_header) {
    int socket_fd = e->data.fd;
    char* remote_filename = client_request_header + 7;
    if (strlen(remote_filename) <= 1) {
        write_all_to_socket(socket_fd, "ERROR\n", strlen("ERROR\n"));
        write_all_to_socket(e->data.fd, err_bad_request, strlen(err_bad_request));
        return;
    }

    remote_filename[strlen(remote_filename)] = '\0'; // Get rid of \n escape character

    char path[strlen(temp_dirname) + strlen(remote_filename) + 2];
    strcpy(path, temp_dirname);
    strcat(path, "/");
    strcat(path, remote_filename);
    
    FILE* remote_file = fopen(path, "r");
    if (remote_file == NULL) {
        write_all_to_socket(socket_fd, "ERROR\n", strlen("ERROR\n"));
        write_all_to_socket(socket_fd, err_no_such_file, strlen(err_no_such_file));
        return;
    }
    
    if (access(path, F_OK) != -1) {
        int delete_status = remove(path);
        if (delete_status == 0) {
             write_all_to_socket(socket_fd, "OK\n", strlen("OK\n"));
            
             // Delete file from vector
             for (size_t i = 0; i < vector_size(server_files); i++) {
                 char* server_file = vector_get(server_files, i);
                 if (strcmp(server_file, remote_filename) == 0) {
                     vector_erase(server_files, i);
                 }
             }

        }
        // else
            // fprintf(stderr, "Delete failed!\n");
    }
    else {
        write_all_to_socket(socket_fd, "ERROR\n", strlen("ERROR\n"));
        write_all_to_socket(socket_fd, err_no_such_file, strlen(err_no_such_file));
    }
}

void accept_connections(struct epoll_event *e, int epoll_fd) {
    while (1) {
      	struct sockaddr_in new_addr;
      	socklen_t new_len = sizeof(new_addr);
      	int new_fd = accept(e->data.fd, (struct sockaddr*) &new_addr, &new_len);

      	if(new_fd == -1) {
            // All pending connections handled
            if(errno == EAGAIN || errno == EWOULDBLOCK)
                break;
            else {
                perror("accept");
                exit(1);
            }
      	}

      	// char *connected_ip= inet_ntoa(new_addr.sin_addr);
      	// int port = ntohs(new_addr.sin_port);

        int flags = fcntl(new_fd, F_GETFL, 0);
        fcntl(new_fd, F_SETFL, flags | O_NONBLOCK);

        //Connection to epoll
        struct epoll_event event;
        event.data.fd = new_fd;
        event.events = EPOLLIN | EPOLLET;
        if (epoll_ctl (epoll_fd, EPOLL_CTL_ADD, new_fd, &event) == -1) {
            perror("accept epoll_ctl");
    	    exit(1);
        }

        // Create a client data structure using client struct
        client new_client;
        new_client.socket_fd = new_fd;
        new_client.client_request_new_header = 0;
        clients[curr_client_num] = new_client;
        // event.data.ptr = new_client;
    }
}

void handle_data(struct epoll_event *e) {
    if (clients[curr_client_num].client_request_new_header == 0) {
        int socket_fd = e->data.fd;
        char client_request_header[CLIENT_REQUEST_SIZE];
        size_t count = 0;
        
        while (1) {
            char c;
            // TODO: add some error handling here
            ssize_t bytes_read = read(socket_fd, &c, 1);
            if (bytes_read == 0) {
                close(socket_fd);
                break;
            }
            else if (bytes_read == -1) {
                if (errno != EAGAIN) {
                    errno = 0;
                    continue;
                }
                break;
            }

            if (c == '\n')
                break;
            client_request_header[count] = c;
            count++;
        }
        if (count == 0)
            return;
        /*
        if (count > 1024) {
            write_all_to_socket(e->data.fd, "ERROR\n", strlen("ERROR\n"));
            write_all_to_socket(e->data.fd, err_bad_request, strlen(err_bad_request));
            close(clients[curr_client_num);
            curr_client_num++;
            return;
        }
        */

        client_request_header[count] = '\0';
        // fprintf(stderr, "Client verb: %s\n", client_request_header);
        clients[curr_client_num].client_request_new_header = 1;

        // 2. Continue reading data from e->data.fd if client sent a PUT request; otherwise, deal with GET, DELETE, LIST accordingly
        if (client_request_header[0] == 'P' && client_request_header[1] == 'U' && client_request_header[2] == 'T' && client_request_header[3] == ' ')
            handle_put_request(e, client_request_header);
        else if (client_request_header[0] == 'G' && client_request_header[1] == 'E' && client_request_header[2] == 'T' && client_request_header[3] == ' ')
            handle_get_request(e, client_request_header);
        else if (client_request_header[0] == 'L' && client_request_header[1] == 'I' && client_request_header[2] == 'S' && client_request_header[3] == 'T')
            handle_list_request(e);
        else if (client_request_header[0] == 'D' && client_request_header[1] == 'E' && client_request_header[2] == 'L' && client_request_header[3] == 'E' && client_request_header[4] == 'T' && client_request_header[5] == 'E' && client_request_header[6] == ' ')
            handle_delete_request(e, client_request_header);
        else { // Else, malformed request
            write_all_to_socket(e->data.fd, "ERROR\n", strlen("ERROR\n"));
            write_all_to_socket(e->data.fd, err_bad_request, strlen(err_bad_request));
        }
            // fprintf(stderr, "About to print out client request header!\n");
            // write(1, client_request, count);
    }

    // 3. After requests of client are handled by server, move on to next client
    // fprintf(stderr, "REACHED HERE\n");
    close(clients[curr_client_num].socket_fd);
    curr_client_num++;
}

void handleControlC() {
    clean_up_temp_directory();
    free(temp_dirname);
    temp_dirname = NULL;
    exit(0);
}

int main(int argc, char **argv) {
    int s;
    // struct epoll_event *events;

    // Create the socket as a nonblocking socket
    int sock_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);

    struct addrinfo hints, *result = NULL;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    char* port_num = argv[1];
    s = getaddrinfo(NULL, port_num, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(1);
    }

    int optval = 1;
    setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    if (bind(sock_fd, result->ai_addr, result->ai_addrlen) != 0 ) {
        perror("bind()");
        exit(1);
    }
    if (listen(sock_fd, MAX_CLIENTS) != 0 ) {
        perror("listen()");
        exit(1);
    }

    // client clients[MAX_CLIENTS];
    curr_client_num = 0;

    struct sockaddr_in sin;
    socklen_t socklen = sizeof(sin);
    if (getsockname(sock_fd, (struct sockaddr *) &sin, &socklen) == -1)
        perror("getsockname");
    // else
        // printf("Listening on port number %d\n", ntohs(sin.sin_port));

    //setup epoll
    int epoll_fd = epoll_create(1);
    if(epoll_fd == -1) {
        perror("epoll_create()");
        exit(1);
    }

    struct epoll_event event;
    event.data.fd = sock_fd;
    event.events = EPOLLIN | EPOLLET; // | EPOLLONESHOT;

    temp_dirname = malloc(7);
    create_temp_directory();
    
    server_files = string_vector_create();    

    //Add the sever socket to the epoll
    if(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, sock_fd, &event)) {
        perror("epoll_ctl()");
        exit(1);
    }

    // Event loop
    while (1) {
        signal(SIGINT, handleControlC);

        struct epoll_event new_event;

        if(epoll_wait(epoll_fd, &new_event, 1, -1) > 0) {
            //Probably check for errors

            // New Connection Ready
            if (sock_fd == new_event.data.fd)
                accept_connections(&new_event, epoll_fd);
            else {
                handle_data(&new_event);
            }
        }
    }

    // Detect a SIGINT and move this up in the main method
    return 0;
}