/**
 * Networking
 */
#include "common.h"
#include "format.h"
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>

#define BUFFER_SIZE 1024
#define HEADER_SIZE 1024
#define SERVER_RESPONSE_SIZE 1024
#define SERVER_LIST_RESPONSE_SIZE 1024
#define SERVER_ERROR_RESPONSE_SIZE 1024

char **parse_args(int argc, char **argv);
verb check_args(char **args);

/* 
 * Helper function to get response from server for PUT and DELETE and print out status of server response accordingly
 */
void read_server_response(int socket_fd) {
    char server_response[SERVER_RESPONSE_SIZE];
    bzero(server_response, SERVER_RESPONSE_SIZE);

    // fprintf(stderr, "About to get PUT/DELETE server response\n");    
    read_all_from_socket(socket_fd, server_response, SERVER_RESPONSE_SIZE); // Short response by server, so it's ok to read it all into a single buffer
    // fprintf(stderr, "PUT/DELETE server response: %s\n", server_response);
    if (server_response[0] == 'E')
        print_error_message(server_response);
    else if (server_response[0] == 'O')
        print_success();
    else
        print_invalid_response();
}

/*
 * Helper function to get response from server for LIST and print out status of server response accordingly 
 * 1. Read from socket the first 
 */
void read_server_list_response(int socket_fd) {
    char server_response[SERVER_LIST_RESPONSE_SIZE];
    bzero(server_response, SERVER_LIST_RESPONSE_SIZE);

    char error_response[SERVER_ERROR_RESPONSE_SIZE];
    bzero(error_response, SERVER_ERROR_RESPONSE_SIZE);

    // 1. Parse list response to get error code and number of bytes
    size_t count = 0;
    while (1) {
        char c;
        read(socket_fd, &c, 1);
        if (c == '\n')
            break;
        error_response[count] = c;
        count++;
    }

    // 2. Now that you have error response, parse it
    if (error_response[0] == 'E') {
        read_all_from_socket(socket_fd, server_response, SERVER_LIST_RESPONSE_SIZE);
        print_error_message(error_response);
        print_error_message(server_response);
    }
    else if (error_response[0] == 'O') {
        // size_t server_response_num_bytes[sizeof(size_t)];
        // bzero(server_response_num_bytes, sizeof(size_t));
       
        char* server_response_num_bytes_char = malloc(sizeof(size_t)); 

        read_all_from_socket(socket_fd, server_response_num_bytes_char, sizeof(size_t));

        size_t bytes_to_be_read = *((size_t*) (server_response_num_bytes_char));
        size_t num_bytes_read = 0;
        while (num_bytes_read < bytes_to_be_read) {
            size_t bytes_read_from_socket = (size_t) read_all_from_socket(socket_fd, server_response, SERVER_LIST_RESPONSE_SIZE);
            if (bytes_read_from_socket == 0) // Finished reading from socket
                break;
            else if ((int) bytes_read_from_socket == -1) {
               if (errno == EINTR || errno == EAGAIN) { // Read failed which means socket was down, so keep trying by resetting errno
                    errno = 0;
                    continue;
               }
               else // Could not read from socket at all
                   break;
            }
            else // Everything worked out
                num_bytes_read += bytes_read_from_socket; 
        }
        if (num_bytes_read < bytes_to_be_read) {
            // fprintf(stderr, "Client LIST request: %lu %lu\n", num_bytes_read, bytes_to_be_read);
            print_too_little_data();
        }
        else if (num_bytes_read > bytes_to_be_read)
            print_received_too_much_data();
        else
            printf("%s", server_response);
    }
    else {
        // fprintf(stderr, "Server LIST response: %s\n", error_response);
        print_invalid_response(); 
    }

}

/**
 * Helper function to get response from server for GET and print out status of server response accordingly
 * 1. Read server response header to see if it's an error or success
 * 2. If error, read_all_from_socket and print the error message returned from server
 * 3. Else, extract the number of bytes and read in a loop using a temp buffer of 1024 bytes and write to local file
 */
void read_server_get_response(int socket_fd, char* local) {
    char server_response[SERVER_LIST_RESPONSE_SIZE];
    bzero(server_response, SERVER_LIST_RESPONSE_SIZE);

    char error_response[SERVER_ERROR_RESPONSE_SIZE];
    bzero(error_response, SERVER_ERROR_RESPONSE_SIZE);

    // 1. Parse list response to get error code and number of bytes
    size_t count = 0;
    while (1) {
        char c;
        read_all_from_socket(socket_fd, &c, 1);
        if (c == '\n')
            break;
        error_response[count] = c;
        count++;
    }

    // 2. Now that you have error response, parse it
    if (error_response[0] == 'E') {
        read_all_from_socket(socket_fd, server_response, SERVER_LIST_RESPONSE_SIZE);
        print_error_message(error_response);
        print_error_message(server_response);
    }

    else if (error_response[0] == 'O') {
        FILE* local_file = fopen(local, "w+");

        size_t server_response_num_bytes[sizeof(size_t)];
        bzero(server_response_num_bytes, sizeof(size_t));

        char* server_response_num_bytes_char = malloc(sizeof(size_t));

        read_all_from_socket(socket_fd, server_response_num_bytes_char, sizeof(size_t));

        size_t num_bytes_read = 0;
        size_t bytes_to_be_read = *((size_t*) (server_response_num_bytes_char));
        while (num_bytes_read < bytes_to_be_read) {
            size_t bytes_read_from_socket = (size_t) read_all_from_socket(socket_fd, server_response, SERVER_LIST_RESPONSE_SIZE);
            if (bytes_read_from_socket == 0) // Finished reading from socket
                break;
            else if ((int) bytes_read_from_socket == -1) {
               if (errno == EINTR || errno == EAGAIN) { // Read failed which means socket was down, so keep trying by resetting errno
                    errno = 0;
                    continue;
               }
               else // Could not read from socket at all
                   break;
            }
            
            num_bytes_read += bytes_read_from_socket; 
            
            int bytes_written = write_all_to_socket(fileno(local_file), server_response, bytes_read_from_socket);
            if (bytes_written == -1) {
                close(socket_fd);
                return;
            }
        }
        if (num_bytes_read < bytes_to_be_read) {
            // if (remove(local) != 0)
                // perror("Error with removing file in too little bytes read GET request");
            print_too_little_data();
        }
        else if (num_bytes_read > bytes_to_be_read) {
            // if (remove(local) != 0)
                // perror("Error with removing file in too many bytes read GET request");
            print_received_too_much_data();
        }
    }
    else
        print_invalid_response();

}

int create_client(char* host, char* port) {
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    struct addrinfo hints, *result;
    memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    int s = getaddrinfo(host, port, &hints, &result);

    if (s != 0) {
        fprintf(stderr, "getaddrinfo %d\n", s);
        exit(1);
    }
    if (connect(socket_fd, result->ai_addr, result->ai_addrlen) == -1) {
        perror(NULL);
        exit(1);
    }

    return socket_fd;
}

void close_connection(int socket_fd) {
    shutdown(socket_fd, SHUT_RDWR);
    close(socket_fd);
}

void get_request_to_server(int socket_fd, char* remote, char* local) {
    // fprintf(stderr, "GET request called\n");
    char header[HEADER_SIZE];
    bzero(header, HEADER_SIZE);

    // 1. First write the header to socket
    sprintf(header, "GET %s\n", remote);
    write_all_to_socket(socket_fd, header, strlen(header));

    // 2. Read the server response and store it as a local file on server
    // 2. Get response from server 
    shutdown(socket_fd, SHUT_WR);
    // FILE* local_file = fopen(local, "w+");
    read_server_get_response(socket_fd, local);
    close_connection(socket_fd);   

    return;
}

void put_request_to_server(int socket_fd, char* remote, char* local) {
    // fprintf(stderr, "PUT request called\n");
    char buffer[BUFFER_SIZE];
    bzero(buffer, BUFFER_SIZE);

    char header[HEADER_SIZE];
    bzero(header, HEADER_SIZE);

    FILE* local_file = fopen(local, "r");
    if (local_file == NULL)
        exit(1);

    // 1. First write the header to socket
    sprintf(header, "PUT %s\n", remote);
    write_all_to_socket(socket_fd, header, strlen(header));

    // 2. Then get the file size and write that to socket
    struct stat local_file_info;
    size_t local_file_size = 0;
    if (stat(local, &local_file_info) != -1) 
        local_file_size = (size_t) local_file_info.st_size;
 
    write_all_to_socket(socket_fd, (char*) &local_file_size, sizeof(size_t));

    // 3. Finally, read the file bytes and write it to the socket right after
    size_t total_num_bytes_read = 0;
    while (total_num_bytes_read < local_file_size) {
        size_t bytes_read_from_socket = (size_t) read_all_from_socket(fileno(local_file), buffer, BUFFER_SIZE);
        // write_all_to_socket(socket_fd, buffer, bytes_read_from_socket);
        if (bytes_read_from_socket == 0) // Finished reading from socket
            break;
        else if ((int) bytes_read_from_socket == -1) {
            fprintf(stderr, "Is this triggered\n");
            if (errno == EINTR || errno == EAGAIN) { // Read failed which means socket was down, so keep trying by resetting errno
                errno = 0;
                continue;
            }
            else // Could not read from socket at all
                break;
        }
        // Else, no error from read
        total_num_bytes_read += bytes_read_from_socket;
        write_all_to_socket(socket_fd, buffer, bytes_read_from_socket);
    }
    bzero(buffer, BUFFER_SIZE);

    // 4. Clear buffer
    shutdown(socket_fd, SHUT_WR);
    read_server_response(socket_fd);
    close_connection(socket_fd);

    return;
}

void delete_request_to_server(int socket_fd, char* remote) {
    // fprintf(stderr, "DELETE request called\n");
    char header[HEADER_SIZE];
    bzero(header, HEADER_SIZE);

    // 1. Construct and send header to server
    sprintf(header, "DELETE %s\n", remote);
    write_all_to_socket(socket_fd, header, strlen(header));

    // 2. Get response from server
    shutdown(socket_fd, SHUT_WR); 
    read_server_response(socket_fd);
    close_connection(socket_fd);

    return;
}

void list_request_to_server(int socket_fd) {
    // fprintf(stderr, "LIST request called\n");
    char header[HEADER_SIZE];
    bzero(header, HEADER_SIZE);

    // 1. Construct and send header to server
    sprintf(header, "LIST\n");
    write_all_to_socket(socket_fd, header, strlen(header));

    // 2. Get response from server 
    shutdown(socket_fd, SHUT_WR);
    read_server_list_response(socket_fd);
    close_connection(socket_fd);

    return;
}

int main(int argc, char **argv) {
    /* Parse the arguments */
    char** parsed_args = parse_args(argc, argv);
    char* host = *(parsed_args + 0);
    char* port = *(parsed_args + 1);
    char* method = *(parsed_args + 2);

    int socket_fd = create_client(host, port);
    if (strcmp(method, "GET") == 0) {
        char* remote = *(parsed_args + 3);
        char* local = *(parsed_args + 4);
        get_request_to_server(socket_fd, remote, local);
    }
    else if (strcmp(method, "PUT") == 0) {
        char* remote = *(parsed_args + 3);
        char* local = *(parsed_args + 4);
        put_request_to_server(socket_fd, remote, local);
    }
    else if (strcmp(method, "DELETE") == 0) {
        char* remote = *(parsed_args + 3);
        delete_request_to_server(socket_fd, remote);
    }
    else if (strcmp(method, "LIST") == 0)
        list_request_to_server(socket_fd);
    else
        printf("%s", err_bad_request);

    return 0;
}

/**
 * Given commandline argc and argv, parses argv.
 *
 * argc argc from main()
 * argv argv from main()
 *
 * Returns char* array in form of {host, port, method, remote, local, NULL}
 * where `method` is ALL CAPS
 */
char **parse_args(int argc, char **argv) {
    if (argc < 3) {
        return NULL;
    }

    char *host = strtok(argv[1], ":");
    char *port = strtok(NULL, ":");
    if (port == NULL) {
        return NULL;
    }

    char **args = calloc(1, 6 * sizeof(char *));
    args[0] = host;
    args[1] = port;
    args[2] = argv[2];
    char *temp = args[2];
    while (*temp) {
        *temp = toupper((unsigned char)*temp);
        temp++;
    }
    if (argc > 3) {
        args[3] = argv[3];
    }
    if (argc > 4) {
        args[4] = argv[4];
    }

    return args;
}

/**
 * Validates args to program.  If `args` are not valid, help information for the
 * program is printed.
 *
 * args     arguments to parse
 *
 * Returns a verb which corresponds to the request method
 */
verb check_args(char **args) {
    if (args == NULL) {
        print_client_usage();
        exit(1);
    }

    char *command = args[2];

    if (strcmp(command, "LIST") == 0) {
        return LIST;
    }

    if (strcmp(command, "GET") == 0) {
        if (args[3] != NULL && args[4] != NULL) {
            return GET;
        }
        print_client_help();
        exit(1);
    }

    if (strcmp(command, "DELETE") == 0) {
        if (args[3] != NULL) {
            return DELETE;
        }
        print_client_help();
        exit(1);
    }

    if (strcmp(command, "PUT") == 0) {
        if (args[3] == NULL || args[4] == NULL) {
            print_client_help();
            exit(1);
        }
        return PUT;
    }

    // Not a valid Method
    print_client_help();
    exit(1);
}